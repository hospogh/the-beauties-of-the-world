import AnalogClock from '../pages/AnalogClock/AnalogClock';
import { IRoute } from '../models/IRoute';
import BinaryClock from '../pages/BinaryClock/BinaryClock';

const routes: IRoute[] = [
  {
    path: '/analog-clock',
    component: AnalogClock,
    title: 'Analog Clock',
  },
  {
    path: '/binary-clock',
    component: BinaryClock,
    title: 'Binary Clock',
  },
];

export default routes;
