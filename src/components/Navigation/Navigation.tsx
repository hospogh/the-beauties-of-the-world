import * as React from 'react';
import styles from './Navigation.module.scss';
import routes from '../../constants/routes';
import { Button } from 'antd';
import { BrowserRouter, Route, Switch, NavLink } from 'react-router-dom';
import { Divider } from 'antd';
import logo from './logo.svg';


export default class Navigation extends React.Component {
  public render() {
    return (
      <BrowserRouter>
        <div className={styles.navigation}>
          <nav className={styles.navigationBar}>
            <NavLink to="/">
              <img className={styles.reactLogo} src={logo} alt="React logo"></img>
            </NavLink>
            {routes.map(r => (
              <NavLink to={r.path}>
                <Button>{r.title}</Button>
              </NavLink>
            ))}
          </nav>
          <Divider></Divider>
          <Switch>
            {routes.map(r => (
              <Route path={r.path}>
                <r.component></r.component>
              </Route>
            ))}
          </Switch>
        </div>
      </BrowserRouter>
    );
  }
}

